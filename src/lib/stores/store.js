import { writable } from 'svelte/store';

function createAppStore(){
    const store = writable({
        theme : 'gerer',
        profile : 'relax',
        solution : 'dokos',
        screen : 'Home',
    });
    const {subscribe, set, update} = store;
    const isBrowser = typeof window !== 'undefined';

    isBrowser &&
      localStorage.appStore &&
      set(JSON.parse(localStorage.appStore));


    return {
        subscribe, 
        set : n => {
            isBrowser && (localStorage.appStore = JSON.stringify(n));
            set(n);
        },
        update : cb => {
            const updatedStore = cb(get(store));

            isBrowser && (localStorage.appStore = JSON.stringify(updatedStore));
            set(updatedStore);
        }
    }
}


export const appStore = createAppStore();