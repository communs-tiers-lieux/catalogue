import matter from 'tiny-matter';

const slugify = function (str) {
    str = str.replace(/^\s+|\s+$/g, ''); // trim
    str = str.toLowerCase();
  
    // remove accents, swap ñ for n, etc
    var from = "àáäâèéëêìíïîòóöôùúüûñç·/_,:;";
    var to   = "aaaaeeeeiiiioooouuuunc------";
    for (var i=0, l=from.length ; i<l ; i++) {
        str = str.replace(new RegExp(from.charAt(i), 'g'), to.charAt(i));
    }

    str = str.replace(/[^a-z0-9 -]/g, '') // remove invalid chars
        .replace(/\s+/g, '-') // collapse whitespace and replace by -
        .replace(/-+/g, '-'); // collapse dashes

    return str;
}

export const getDataFromMainPad = async ()=> {

    const content = await fetch('https://pad.lescommuns.org/communs-tiers-lieux/download');
    const markdown = await content.text();
    const metadata = matter(markdown).data;

    let result = {};

    result.intro = metadata.intro;

   result.themes = Object.entries(metadata.themes).map( ([name, description]) => {
    return { id : slugify(name), label : name, description : description}
   })

   result.profiles = Object.entries(metadata.profils).map( ([name, description]) => {
    return { id : slugify(name), label : name, description : description}
   })

   result.solutions = Object.entries(metadata.solutions).map( ([name, params]) => {
    return {
        id : params.permalien,
        label : name,
        description : params.description,
        profiles : params.profils.map( i => slugify(i) ),
        themes : params.themes.map( i => slugify(i) )
    }
   })

   return result
}


// export const source = {
//     themes : [
//       { id : 'gerer', label : "Gérer" , description : 'Des communs pour administrer votre tiers-lieu : comptabilité, planning, réservation...'},
//       { id : 'animer', label : "Animer" , description : 'Faire vivre votre communauté '},
//       { id : 'documenter', label : "Documenter" , description : 'Documenter vos pratiques est essentiel. Faites le avec les bons outils'},
//       { id : 'partager', label : "Partager" , description : 'Des communs pour mettre en communs...'},
//       { id : 'travailler', label : "Travailler" , description : 'Des outils de productivités pour vous et votre communauté'}
//     ],
//     profiles : [
//       { id : 'relax', label : "Relax" , description : 'Vous démarrez votre projet ou êtes sur un rythme de croisière. Vous voulez jeter un oeil sur ce qui existe et pourrait vous servir plus tard.'},
//       { id : 'presse', label : "Pressé" , description : "Besoin d'une solution rapidement ? Quelque chose de simple, rapide à mettre en place et à utiliser !"},
//       { id : 'robuste', label : "Robuste" , description : 'On construit pour durer même si cela veut dire prendre le temps de dompter une solution plutôt "costaud" !'}
//     ],
//     solutions : [
//       { id : 'dokos', label : 'Dokos', description : "Dokos est un ERP. Cela signifie en anglais Enterprise Resource Planning. Un ERP est un logiciel de gestion utilisé pour collecter, stocker, gérer, interpréter les données des activités commerciales, appliquer les processus, activer les mécanismes de contrôle et automatiser une gamme d'activités.", themes : ['gerer',], profiles : ['robuste']},
//     ]
//   }
